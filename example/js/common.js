// 共用的地方
// 可能會影響其他人

//爆炸效果
function get_explosion(x, y) { //爆炸
    // Get the first dead explosion from the explosionGroup
    var explosion = this.explosionGroup.getFirstDead();

    // If there aren't any available, create a new one
    if (explosion === null) {
        explosion = this.game.add.sprite(0, 0, 'explosion');
        explosion.anchor.setTo(0.5, 0.5);

        // Add an animation for the explosion that kills the sprite when the
        // animation is complete
        var animation = explosion.animations.add('boom', [0, 1, 2, 3], 60, false);
        animation.killOnComplete = true;

        // Add the explosion sprite to the group
        this.explosionGroup.add(explosion);
    }

    // Revive the explosion (set it's alive property to true)
    // You can also define a onRevived event handler in your explosion objects
    // to do stuff when they are revived.
    explosion.revive();

    // Move the explosion to the given coordinates
    explosion.x = x;
    explosion.y = y;

    // Set rotation of the explosion at random for a little variety
    explosion.angle = this.game.rnd.integerInRange(0, 360);

    // Play the animation
    explosion.animations.play('boom');

    // Return the explosion itself in case we want to do anything else with it
    return explosion;
};


function add_score(num) {
    score = score + parseInt(num);
}


//取得當前速度
function get_final_speed() {
    //game.camera.shake(0.02, 600);
    return 10 * speedHold + 15 * speedCombo;
}

//取得影格速度
function get_frame_speed() {
    return (speedHold + speedCombo) > 20 ? (speedHold + speedCombo) : 20;
}

//遊戲死亡
function game_over(msg) {
    game_finish(0,msg);
}

//遊戲win
function game_win(msg) {
    game_finish(1,msg);
}

function game_finish(flag,msg) {
    game.time.events.remove(timer_time_event);
    if (state == 'DIE') {
        return;
    }
    state = 'DIE';
    player.kill();

    s_bg.pause();
    s_all.pause();

    /*destroy */
    game.input.keyboard.stop;


    if(flag >= 1){
        //顯示遊戲結束文本
        infoText.text = msg + "\nClick to restart";
        s_all.play('youWin');
    } else{
        //顯示遊戲結束文本
        infoText.text = msg + "\nClick to restart";
        s_all.play('gameOver');
    }
    infoText.x = player.x;
    infoText.y = player.y - 100;
    infoText.visible = true;


    //按鈕
    button = game.add.button(player.x - 95, player.y, 'button', restart, this, 2, 2, 2);
}


function restart() {
    game.state.start('GameJam');
}



//計時相關 都用100毫秒為基準
function timer() {
    if (state == 'DIE') {
        return;
    }

    timeStop++;
    timeWalk++;
    timeSound++;
    timeHeartbeat++;
    timeCheckHeartBeat++;
    timeScore++;

    if (timeStop >= 20) {
        if (speedCombo > 1) {
            speedCombo--;
            timeStop = 0;
        }
    }

    if (timeWalk >= 5 && speedHold < 15) {
        speedHold++;
    }

    if (timeSound >= 10) {
        //heart_beat_sound('1', get_final_speed());
        heart_beat_sound('2', heartBeat);
        timeSound = 0;
    }

    if(!heartbeatFlag){
        //if(get_final_speed() > 500){
        //    heartbeat_blink(1);
        //}else if(get_final_speed() > 1000){
        //    heartbeat_blink(2);
        //}
        if(heartBeat > 200){
            heartbeat_blink(2);
        }else if(heartBeat > 100){
            heartbeat_blink(1);
        }
    }

    if (timeCheckHeartBeat >= 3) {
        check_heart_beat();
        timeCheckHeartBeat = 0;
    }

    if(timeScore >= 10){
        var nowSpeed = get_final_speed();
        var speedTran = Math.ceil(nowSpeed / 1000);
        timeScore = 0;
        add_score(speedTran);
    }
    if(backMask.alpha < 0.4){
        backMask.alpha += 0.001;
    }
}
