//var game = new Phaser.Game(bg_X, bg_Y, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render});
var player;
var cursors;
var speedHold = 1; //持續按壓
var speedCombo = 1; //連按速度
var lockSpeed = 0;
var timeStop = 0; //靜止不動的時間
var timeWalk = 0; //開始走路的時間
var timeSound = 0; //心跳音效的時間
var timeHeartbeat = 0; //心跳時間
var timeCheckHeartBeat = 0; //檢查玩家心跳時間
var timeScore = 0; //時間計數
var state = 'STOP';  //WALK & STOP & DIE
var infoText; //訊息
var explosionGroup; //爆炸
var backGround; //背景
var backMask; //背景遮罩 red
var blink = 1; //心跳閃動
var heartbeatFlag = 0; //開啟心跳畫面
var button;
var score = 0;
var bloodyMask;

/* 障礙物 */
var obstacle;


/* 子彈 */
var bullets;    // 子彈
var fireButton; // 發射
var bulletTime = 0;
var direction = 0; //射擊方向  0 上 1 下 2 左 3 右
var gunbox;
var Ammos = 0;

/* 心跳區 */
var heartBeatMin = 60;
var heartBeatMax = 300;
var heartBeat = heartBeatMin;
var heartBeatKeepMin = 200; //心跳維持 一次5分
var heartBeatKeepMax = 220; //心跳維持 一次5分
var heartBeatAdd1 = heartBeatMin;
var heartBeatAdd2 = 0;
//存活時間
var aliveTime = 0;
var tempSpeed = 0; //暫存上次速度
var timer_time_event;

// 火焰生成相關
var fire_num = 0;
var fire_rnd_add_value = 30;
var fire_upgrade_need_value = 20;
var fire;
var fire_roaming_value = 0;
var fire_roaming_need_value = 1000;
var fire_rnd_add_roaming_value = 1000;
var fire_random_distance = 500;
var fireLocation = [];
var fire_roaming_add_heartbeat = 0;
var fire_per_heartbeat = 30;
var fire_eat_card_add_heartbeat = 10;



//聲音
var number = 0;

//音效
var s_all;
var s_bg;

// 卡牌
var cardMax = 16;
var cardLocation = [];
var card;
var card_num = 0;
var card_list = [];
var eat_card_score = 100;
var card_shadow;

//手遊Button
var buttonUp;
var buttonDown;
var buttonLeft;
var buttonRight;
var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
var GameJam = function(game) {};

GameJam.prototype = {
//加載資源
preload: function() {
},





 init: function() {
    game.state.restart();

    player;
    cursors;
    speedHold = 1; //持續按壓
    speedCombo = 1; //連按速度
    lockSpeed = 0;
    timeStop = 0; //靜止不動的時間
    timeWalk = 0; //開始走路的時間
    timeSound = 0; //心跳音效的時間
    timeHeartbeat = 0; //心跳時間
    timeCheckHeartBeat = 0; //檢查玩家心跳時間
    timeScore = 0; //時間計數
    state = 'STOP';  //WALK & STOP & DIE
    infoText; //訊息
    explosionGroup; //爆炸
    backGround; //背景
    backMask; //背景遮罩 red
    blink = 1; //心跳閃動
    heartbeatFlag = 0; //開啟心跳畫面
    button;
    score = 0;
    bloodyMask;

    /* 障礙物 */
    obstacle;

    /* 子彈 */
    bullets;    // 子彈
    fireButton; // 發射
    bulletTime = 0;
    direction = 0; //射擊方向  0 上 1 下 2 左 3 右
    gunbox;
    Ammos = 0;

    /* 心跳區 */
    heartBeatMin = 60;
    heartBeatMax = 300;
    heartBeat = heartBeatMin;
    heartBeatKeepMin = 100; //心跳維持 一次5分
    heartBeatKeepMax = 120; //心跳維持 一次5分
    heartBeatAdd1 = heartBeatMin;
    heartBeatAdd2 = 0;

    //存活時間
    aliveTime = 0;
    tempSpeed = 0; //暫存上次速度
    timer_time_event;

    // 火焰生成相關
    fire_num = 0;
    fire_rnd_add_value = 30;
    fire_upgrade_need_value = 20;
    fire;
    fire_roaming_value = 0;
    fire_roaming_need_value = 1000;
    fire_rnd_add_roaming_value = 1000;
    fire_random_distance = 500;
    fireLocation = [];
    fire_roaming_add_heartbeat = 0;
    fire_per_heartbeat = 20;
    fire_eat_card_add_heartbeat = 20;

    //聲音
    number = 0;

    //音效
    s_all;
    s_bg;

    // 卡牌
    cardMax = 16;
    cardLocation = [];
    card_num = 0;
    $eat_number = 0;
    card;
    card_list = [];
    eat_card_score = 100;
    card_shadow;

     //手遊Button
     buttonUp;
     buttonDown;
     buttonLeft;
     buttonRight;
 },

//創建場景
 create: function() {
    /*    地圖生成    */
    for (var i = 0; i <= bg_X; i+=500) {
        backGround = game.add.tileSprite(0, 0, i + 500, i + 500, 'backGround');
    }

    backGround.fixedToCamera = true;
    //火焰
    for (var i = 0; i < cardMax/4; i++) {
        for (var j = 0; j < cardMax/4; j++) {
            var K = [];
            for(var k = 0; k < game.rnd.integerInRange(0, 3); k++){
                var  L = Array(game.rnd.integerInRange(bg_X/4*(j%4), bg_X/4*((j%4)+1)), game.rnd.integerInRange(bg_Y/4*(i%4), bg_Y/4*((i%4)+1)));
                K.push(L);
            }
            fireLocation.push(K);
        }
    }
    //卡牌
    for (var i = 0; i < cardMax/4; i++) {
        for (var j = 0; j < cardMax/4; j++) {
            var X = game.rnd.integerInRange(bg_X/4*(j%4)+50, bg_X/4*((j%4)+1)-50);
            var Y = game.rnd.integerInRange(bg_Y/4*(i%4)+50, bg_Y/4*((i%4)+1)-50);
            cardLocation.push([X, Y]);
        }
    }
     cardLocation = this.shuffle(cardLocation);
    /******************/

    s_bg = game.add.audio('s_bg');
    s_bg.play();

    s_all = game.add.audio('s_all');
    s_all.allowMultiple = false;
    //s_all.addMarker('die', 0.0, 0.56);
    s_all.addMarker('hitFire', 0.6, 0.48);
    s_all.addMarker('getItem', 1.1, 0.13);
    s_all.addMarker('gameOver', 1.25, 1.25);
    s_all.addMarker('heartBeat1', 2.5, 1.98);
    s_all.addMarker('heartBeat2', 4.7, 1.6);
    s_all.addMarker('heartBeat3', 6.33, 1.0);
    s_all.addMarker('getCard', 7.5, 1.3);
    s_all.addMarker('getWrong', 8.4, 0.5);
    s_all.addMarker('youWin', 9.0, 2.0);

    game.world.setBounds(0, 0, bg_X, bg_Y); //地圖大小
    game.physics.startSystem(Phaser.Physics.ARCADE);

    player = game.add.sprite(bg_X / 2, bg_Y / 2, 'player', 1);

    game.camera.follow(player);
    //  We need to enable physics on the player
    game.physics.arcade.enable(player);

    //  Player physics properties. Give the little guy a slight bounce.
    player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    player.animations.add('up', [36, 38, 37]);
    player.animations.add('down', [0, 2, 1]);
    player.animations.add('left', [12, 14, 13]);
    player.animations.add('right', [24, 26, 25])

    game.physics.arcade.sortDirection = Phaser.Physics.Arcade.RIGHT_LEFT;



    init_create_obstacle(bg_X, bg_Y);//障礙物

    //生牌
    init_create_card(bg_X, bg_Y);

    //生火
    init_create_fire(bg_X, bg_Y);

    // cursors
    cursors = game.input.keyboard.createCursorKeys();

    //計時器 毫秒
    timer_time_event = game.time.events.loop(100, timer, this);

    //提示信息
    infoText = game.add.text(player.x, player.y, ' ', { font: '24px Arial', fill: '#fff' });
    infoText.anchor.setTo(0.5, 0.5);
    infoText.visible = false;

    //爆炸
    explosionGroup = game.add.group();

    init_bullets(); // 子彈
    create_gunbox(300, 500); // 產生箱子




    fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

     //我要在最下面
     //手遊按鈕
     if(isMobile){
        buttonUp = game.add.sprite(bg_X-410, bg_Y-450, 'buttonUp', 0);
        buttonUp.width = 205;
        buttonUp.height = 205;
        game.physics.arcade.enable(buttonUp);
        buttonUp.inputEnabled = true;
        buttonUp.input.pixelPerfectClick = true;
        buttonUp.events.onInputDown.add(this.playerUp,this);

        buttonDown = game.add.sprite(bg_X-410, bg_Y-205, 'buttonDown', 15);
        buttonDown.width = 205;
        buttonDown.height = 205;
        game.physics.arcade.enable(buttonDown);
        buttonDown.inputEnabled = true;
        buttonDown.input.pixelPerfectClick = true;
        buttonDown.events.onInputDown.add(this.playerDown,this);

        buttonLeft = game.add.sprite(bg_X-600, bg_Y-330, 'buttonLeft', 4);
        buttonLeft.width = 205;
        buttonLeft.height = 205;
        game.physics.arcade.enable(buttonLeft);
        buttonLeft.inputEnabled = true;
        buttonLeft.input.pixelPerfectClick = true;
        buttonLeft.events.onInputDown.add(this.playerLeft,this);

        buttonRight = game.add.sprite(bg_X-220, bg_Y-330, 'buttonRight', 3);
        buttonRight.width = 205;
        buttonRight.height = 205;
        game.physics.arcade.enable(buttonRight);
        buttonRight.inputEnabled = true;
        buttonRight.input.pixelPerfectClick = true;
        buttonRight.events.onInputDown.add(this.playerRight,this);
     }
    backMask = game.add.image(0, 0, 'mask');
    backMask.alpha = 0;
},


//更新場景
 update: function() {
    if (state == 'DIE') {
        return;
    }
    game.physics.arcade.overlap(player, fire, collisionHandler, null, this);
    game.physics.arcade.overlap(bullets, fire, extinguishingHandler, null, this);
    game.physics.arcade.overlap(player, gunbox, getWaterGunHandler, null, this);
    game.physics.arcade.collide(player, obstacle, obstacleHandler, null, this);
    game.physics.arcade.collide(player, card, cardsHandler, null, this);


    //  Reset the players velocity (movement)
    player.body.velocity.x = 0;
    player.body.velocity.y = 0;

    var walking = false;
    var walking_animation = null;

    if (cursors.up.isDown) {
        state = 'WALK';
        //  Move to the left
        player.body.velocity.y = -15;
        player.y -= 4;

        if (speedHold >= 15 && lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        player.body.velocity.y = -1 * get_final_speed();
        walking = true;
        walking_animation = 'up';
        direction = 0;
    }
    else if (cursors.down.isDown) {
        state = 'WALK';
        //  Move to the left
        player.body.velocity.y = 15;
        player.y += 4;

        if (speedHold >= 15 && lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        player.body.velocity.y = 1 * get_final_speed();
        walking = true;
        walking_animation = 'down';
        direction = 1;
    }

    if (cursors.left.isDown) {
        state = 'WALK';
        //  Move to the left
        player.body.velocity.x = -15;
        player.x -= 4;

        if (speedHold >= 15 && lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        player.body.velocity.x = -1 * get_final_speed();
        walking = true;
        walking_animation = 'left';
        direction = 2;
    }
    else if (cursors.right.isDown) {
        state = 'WALK';
        //  Move to the right
        player.body.velocity.x = 15;
        player.x += 4;

        if (speedHold >= 15 && lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        player.body.velocity.x = 1 * get_final_speed();
        walking = true;
        walking_animation = 'right';
        direction = 3;
    }

    if (walking) {
        state = 'WALK';
        timeStop = 0;
        frame_speed = get_frame_speed();
        player.animations.play(walking_animation, frame_speed, false)
    } else {
        state = 'STOP';
        lockSpeed = 0;
        timeWalk = 0;
    }

    // 開槍
    if (fireButton.isDown)
    {
        fireBullet();
    }

},

 render: function() {
    var finalSpeed = get_final_speed();
    game.debug.text("Speed: " + finalSpeed, 32, 32 * 1);
    game.debug.text("Score: " + score, 32, 32 * 2);
    game.debug.text("Heart Beat: " + heartBeat, 32, 32 * 3);
    game.debug.text("Bullet Ammos: " + Ammos, 32, 32 * 4);
},
shuffle: function(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
},
//手遊按鈕
playerUp: function(){
    state = 'WALK';
    //  Move to the left
    //player.body.velocity.y = -15;
    player.y -= 10;

    if (lockSpeed <= 0) {
        speedCombo++;
        lockSpeed = 1;
    }
    //player.body.velocity.y = -1 * get_final_speed();
    walking = true;
    walking_animation = 'up';
    direction = 0;
},
    playerDown: function(){
        state = 'WALK';
        //  Move to the left
        //player.body.velocity.y = 15;
        player.y += 10;

        if (lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        //player.body.velocity.y = 1 * get_final_speed();
        walking = true;
        walking_animation = 'down';
        direction = 1;
    },
    playerLeft: function(){
        state = 'WALK';
        //  Move to the left
        //player.body.velocity.x = -15;
        player.x -= 10;

        if (lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        //player.body.velocity.x = -1 * get_final_speed();
        walking = true;
        walking_animation = 'left';
        direction = 2;
    },
    playerRight: function(){
        state = 'WALK';
        //  Move to the right
        //player.body.velocity.x = 15;
        player.x += 10;

        if (lockSpeed <= 0) {
            speedCombo++;
            lockSpeed = 1;
        }
        //player.body.velocity.x = 1 * get_final_speed();
        walking = true;
        walking_animation = 'right';
        direction = 3;
    },


};
