// obstacle

var stack_keys = [
    ['log_small_NE.png','small_stack'],
];

var rock_keys = [
    ['rock_large1_NE.png','large'],
    ['rock_smallTop1_NW.png','small'],
    ['rock_tall3_SE.png','tall'],
    ['stone_small7_NE.png','small'],
    ['stone_smallFlat3_NE.png','small'],
    ['stone_smallTop1_SE.png','small'],
    ['stone_tall5_SW.png','tall'],
];

var tree_keys = [
    ['tree_blocks_dark_SE.png','small'],
    ['tree_blocks_fall_SW.png','small'],
    ['tree_blocks_SE.png','small'],
    ['tree_default_fall_NW.png','small'],
    ['tree_default_NW.png','small'],
    ['tree_thin_SW.png','small'],
];

var obstacle_size = {
    'small_stack': [16,16],
    'small': [32,32],
    'tall' : [32,48],
    'large': [48,48],
};

function preload_obstacle() {
    stack_keys.forEach(function(element) {
        game.load.image(element[0], 'assets/obstacle/'+element[0]);
    });

    rock_keys.forEach(function(element) {
        game.load.image(element[0], 'assets/obstacle/'+element[0]);
    });

    tree_keys.forEach(function(element) {
        game.load.image(element[0], 'assets/obstacle/'+element[0]);
    });

    game.load.start();
}

function init_create_obstacle(bg_X, bg_Y) {
    obstacle = game.add.physicsGroup(Phaser.Physics.ARCADE);

    obstacle.enableBody = true;
    obstacle.physicsBodyType = Phaser.Physics.ARCADE;

    var xs ,ys ;

    var stack_key = stack_keys.length;

    // 木頭 數量
    for (var i = 0; i < 10; i++)
    {
        var key = game.rnd.integerInRange(0, stack_key - 1);
        xs = game.rnd.integerInRange(10, bg_X);
        ys = game.rnd.integerInRange(10, bg_Y);
        if(check_xy(xs,ys)==1){
            continue;
        };

        //var c = group.create(game.rnd.integerInRange(200, 1900), game.rnd.integerInRange(0, 1100), 'veggies', game.rnd.integerInRange(0, 35));
        var stack = obstacle.create( xs, ys, stack_keys[key][0] );
        stack.body.immovable = true;
        stack.width = obstacle_size[stack_keys[key][1]][0];
        stack.height = obstacle_size[stack_keys[key][1]][1];
    }

    var rock_key = rock_keys.length;

    // 石頭 數量
    for (var i = 0; i < 15; i++)
    {
        var key = game.rnd.integerInRange(0, rock_key - 1);
        xs = game.rnd.integerInRange(10, bg_X);
        ys = game.rnd.integerInRange(10, bg_Y);
        if(check_xy(xs,ys)==1){
            continue;
        };

        //var c = group.create(game.rnd.integerInRange(200, 1900), game.rnd.integerInRange(0, 1100), 'veggies', game.rnd.integerInRange(0, 35));
        var rock = obstacle.create( xs, ys, rock_keys[key][0] );
        rock.body.immovable = true;
        rock.width = obstacle_size[rock_keys[key][1]][0];
        rock.height = obstacle_size[rock_keys[key][1]][1];

    }
    var tree_key = tree_keys.length;

    // 石頭 數量
    for (var i = 0; i < 90; i++)
    {
        var key = game.rnd.integerInRange(0, tree_key - 1);
        xs = game.rnd.integerInRange(10, bg_X);
        ys = game.rnd.integerInRange(10, bg_Y);
        if(check_xy(xs,ys)==1){
            continue;
        };

        //var c = group.create(game.rnd.integerInRange(200, 1900), game.rnd.integerInRange(0, 1100), 'veggies', game.rnd.integerInRange(0, 35));
        var tree = obstacle.create( xs, ys, tree_keys[key][0] );
        tree.body.immovable = true;
        tree.width = obstacle_size[tree_keys[key][1]][0];
        tree.height = obstacle_size[tree_keys[key][1]][1];
    }
}

function check_xy(xs,ys){
    var a = 0;
    cardLocation.forEach(function(element){

        var x = Math.abs(xs - element[0]);
        var y = Math.abs(ys - element[1]);

        if(parseInt(x) < 50 && parseInt(y) < 50){
            a=1;
            return ;
        }
    });

        var x = Math.abs(xs - (bg_X / 2));
        var y = Math.abs(ys - (bg_Y / 2));
        	console.log('x'+ bg_X + 'y' + bg_Y);

        if(parseInt(x) < 80 && parseInt(y) < 80){
            a=1;
            return a;
        }

    return a;
}

function obstacleHandler (){
    // do nothing
}
