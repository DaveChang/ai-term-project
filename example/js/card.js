function init_create_card(X, Y) {

    card = game.add.physicsGroup(Phaser.Physics.ARCADE);

    for (var i = 0; i < 13; i ++) {
        var card_location = cardLocation[i];
        var X = card_location[0];
        var Y = card_location[1];
        card_list[i] = initial_card_obj(card, X, Y);
    }

    card_list[0].shadow.kill();
}

var card_frame = [0,50,19,37,30,23,16,9,2,64,57,43,29,36,22];

function initial_card_obj(card, X, Y) {
    var num = ++card_num;

    var obj = card.create(
        X, Y, 'cards', card_frame[num]
    );
    console.log(num);

    obj.num = num;
    obj.width = 35;
    obj.height = 47;
    //obj.name = 'card'+num;
    obj.body.immovable = true;

    var card_shadow = game.add.sprite(X, Y, 'cards', card_frame[num]);
    card_shadow.tint = 0x000000;
    card_shadow.alpha = 0.5;
    card_shadow.width = 35;
    card_shadow.height = 47;
    obj.shadow = card_shadow;

    // 增加fire升級，越高階的越容易碰到，只會升階不會降，到第三階開始才會蔓延
    return obj;
}

function cardsHandler(player, card){

    //console.log(card);

    var next_num = card_list[0].num;

    if (card.num === next_num)
    {
        add_score(eat_card_score);
        card_list.shift();
        card.kill();

        if(card_list.length === 0){
            // win
            game_win('恭喜過關！');
        } else {
            s_all.play('getCard');
            if (card_list[0].shadow) {
                card_list[0].shadow.kill();
            }
        }
        // 吃到 9 增加火焰
        if(card.num == 9){
            fire_per_heartbeat*=3600;
            fire_rnd_add_value*=2400;
            fire_upgrade_need_value/=4;
        }
        // 吃到 11 增加火焰
        if(card.num == 11){
            fire_per_heartbeat*=3;
            fire_rnd_add_value*=3;
            fire_upgrade_need_value/=4;
        }
    }else{
        s_all.play('getWrong');
    }


}
