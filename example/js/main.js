var bg_X = 1880;
var bg_Y = 1040;
var game = new Phaser.Game(bg_X, bg_Y, Phaser.AUTO, 'game'), Main = function () {};
var gameOptions = {
    playSound: true,
    playMusic: true
  };
var musicPlayer;
Main.prototype = {
    preload: function () {
        game.load.image('stars',    'assets/images/stars.jpg');
        game.load.image('loading',  'assets/images/loading.png');
        game.load.image('brand',    'assets/images/logo.png');
        game.load.script('utils',   'lib/utils.js');
        game.load.script('splash',  'states/Splash.js');

        //手遊按鈕
        game.load.spritesheet('buttonUp', 'assets/controllers/xbox360.png', 102.5, 102.5, 19);//按鈕
        game.load.spritesheet('buttonDown', 'assets/controllers/xbox360.png', 102.5, 102.5, 19);//按鈕
        game.load.spritesheet('buttonLeft', 'assets/controllers/xbox360.png', 102.5, 102.5, 19);//按鈕
        game.load.spritesheet('buttonRight', 'assets/controllers/xbox360.png', 102.5, 102.5, 19);//按鈕

        // Game Assets preload here
        game.load.image('backGround', 'assets/grass.jpg');
        game.load.image('mask', 'assets/pics/mask-1.png');
        game.load.image('bloody1', 'assets/pics/bloody-frame-1.png');
        game.load.image('bloody2', 'assets/pics/bloody-frame-2.png');
        game.load.image('bloody3', 'assets/pics/bloody-frame-3.png');
        game.load.image('bloody4', 'assets/pics/bloody-frame-4.png');
        game.load.image('bloody5', 'assets/pics/bloody-frame-5.png');

        game.load.image('bullet', 'assets/bullets/bullet77.png');
        game.load.image('gunbox', 'assets/sprites/firstaid.png');

        game.load.spritesheet('player', 'assets/character.png', 32, 32, 96);
        game.load.spritesheet('fire', 'assets/fire1.png', 75, 64, 16);
        game.load.spritesheet('explosion', 'assets/gfx/explosion.png', 128, 128); //爆炸
        game.load.spritesheet('button', 'assets/buttons/button_sprite_sheet.png', 193, 71);//按鈕

        game.load.spritesheet('cards', 'assets/sprites/playingCards.png', 140, 190); //卡牌

        game.load.audio('s_all', 'assets/audio/s_all.mp3');
        game.load.audio('s_bg', 'assets/audio/s_bg.mp3');
        game.load.script('obstacle','js/obstacle.js');
    },

    create: function () {
        game.state.add('Splash', Splash);
        game.state.start('Splash');
    }

};

game.state.add('Main', Main);
game.state.start('Main');
