var Story = function() {};
var text;
Story.prototype = {
    init: function () {

    },
    create: function () {
        var style = { font: "65px Arial", fill: "#ff0044", align: "center" };
        //var style1 = { font: "65px Arial", fill: "#ff0044", align: "center" };
        var story = "逐漸燃燒的大地，舉辦著一場玩命的心臟病遊戲\n玩家需尋找散落的13張牌來完成任務，獲勝即可得到豐富獎金!\n為了得到獎金，患有心臟病的你賭上一切報名參加...";
        var rule = "\n\n蒐集至少10張牌\n\n-注意你的心跳\n\n- 小心火焰";
        text = game.add.text(game.world.centerX, game.world.centerY, story, style);

        text.anchor.set(0.5);
        text.inputEnabled = true;

        text.events.onInputOver.add(over, this);
        text.events.onInputOut.add(out, this);

        this.addMenuOption('- Skip', function (e) {
            game.state.start("GameJam");
        });
        
        game.input.onDown.addOnce(rule1);
    },
    addMenuOption: function(text, callback) {
        var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'right', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
        var txt = game.add.text(game.world._width - 150, game.world._height - 100, text, optionStyle);
        txt.events.onInputOver.add(over, this);
        txt.events.onInputOut.add(out, this);
        txt.inputEnabled = true;
        txt.events.onInputUp.add(callback);
    }
};

function over(item) {

    item.fill = "#ffff44";

}

function out(item) {

    item.fill = "#ff0044";

}
function rule1(){
    text.fill = "#ffff44";
    text.setText('蒐集至少10張牌');
    game.input.onDown.addOnce(rule2);
}

function rule2(){
    text.fill = "#ffff44";
    text.setText('注意你的心跳');
    game.input.onDown.addOnce(rule3);
}

function rule3(){
    text.fill = "#ffff44";
    text.setText('小心火焰...');
    game.input.onDown.addOnce(startGame);
}

function startGame() {
    game.state.start('GameJam');
}
