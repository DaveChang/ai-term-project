function init_create_fire(X, Y) {
    // 火焰
    fire = game.add.physicsGroup(Phaser.Physics.ARCADE);

    // 這邊做初始化每格火焰生成位置，先隨便給，根據Dave給的值生成
    for (var i = 0; i < 16; i ++) {
        var fire_location = fireLocation[i];
        for (var j = 0; j < fire_location.length; j++) {
            var X = fire_location[j][0];
            var Y = fire_location[j][1];
            if (!(X < 6 + player.x && player.x < X + 50 && player.y + 17 > Y && player.y - 60 < Y)) {
                var fire_obj = initial_fire_obj(fire, X, Y);
            }
        }
    }

}

function fire_up(obj) {
    var value = obj.upgrade_value;
    if (obj.upgrade_value > fire_upgrade_need_value) {
        obj.upgrade_value = 0;
        if (obj.stage < 4) {     // 最高到四階
            obj.stage += 1;
            var fire_animations = 'fire_stage_'+obj.stage;
            obj.animations.play(fire_animations, 10, true);
        }
        else {
            // 做蔓延，隨機往外擴張
            fire_roaming_add_heartbeat ++ ;

            if(fire_roaming_add_heartbeat / fire_per_heartbeat > 1) {
                add_heart_beat(fire_roaming_add_heartbeat / fire_per_heartbeat);
                fire_roaming_add_heartbeat = 0;
            }
            game.time.events.remove(obj.time_events);
            var X = obj.x + game.rnd.integerInRange(-50, 50);
            var Y = obj.y + game.rnd.integerInRange(-50, 30);

            var roaming_fire = initial_fire_obj(obj.parent, X, Y);
        }
        judge_eat_card(obj);
    }
    var add_value = game.rnd.integerInRange(1, fire_rnd_add_value);
    obj.upgrade_value += add_value;
}

function initial_fire_obj(fire, X, Y) {
    var num = ++fire_num;
    var obj = fire.create(
        X, Y, 'fire',
    );
    obj.name = 'fire'+num;
    obj.body.immovable = true;
    obj.animations.add('fire_stage_1', [0, 1, 2, 3]);
    obj.animations.add('fire_stage_2', [4, 5, 6, 7]);
    obj.animations.add('fire_stage_3', [8, 9, 10, 11]);
    obj.animations.add('fire_stage_4', [12, 13, 14, 15]);

    // 增加fire升級，越高階的越容易碰到，只會升階不會降，到第三階開始才會蔓延
    obj.upgrade_value = 0;
    obj.stage = 1;
    obj.animations.play('fire_stage_1', 10, true);
    obj.time_events = game.time.events.loop(1000, fire_up, this, obj);
    return obj;
}


//火焰碰撞
function collisionHandler(player, fire) {

    var collide = false;
    if (fire.stage == 1 && fire.x < 6 + player.x && player.x < fire.x + 50 && player.y + 17 > fire.y && player.y - 60 < fire.y) {
        collide = true;
    } else if (fire.stage == 2 && fire.x < 6 + player.x  && player.x < fire.x + 45 && player.y + 30 > fire.y && player.y - 60 < fire.y) {
        collide = true;
    } else if (fire.stage == 3 && fire.x < 22 + player.x && player.x < fire.x + 46 && player.y + 30 > fire.y && player.y - 60 < fire.y) {
        collide = true;
    } else if (fire.stage == 4 && fire.x < 22 + player.x && player.x < fire.x + 86 && player.y + 30 > fire.y && player.y - 60 < fire.y) {
        collide = true;
    }

    //  If the player collides with the chillis then they get eaten :)
    //  The chilli frame ID is 17
    if (collide) {
        fire.kill();
        number--;
        //爆炸
        get_explosion(player.x, player.y);
        game_over('死於火焰燒毀'); //XXX
    }
}

function judge_eat_card(fire) {
    for (var i = 0; i < card_list.length; i ++) {
        var num = card_list[i].num;
        var cardX = card_list[i].x;
        var cardY = card_list[i].y;

        if (fire.stage == 1 && fire.x + 51 > cardX && fire.x - 11 < cardX && fire.y + 57 > cardY && fire.y - 28 < cardY)  {
            fire_eat_card(card_list[i], i);
        } else if (fire.stage == 2 && fire.x + 51 > cardX && fire.x - 11 < cardX && fire.y + 61 > cardY && fire.y - 47 < cardY) {
            fire_eat_card(card_list[i], i);
        } else if (fire.stage == 3 && fire.x + 49 > cardX && fire.x - 28 < cardX && fire.y + 63 > cardY && fire.y - 47 < cardY) {
            fire_eat_card(card_list[i], i);
        } else if (fire.stage == 4 && fire.x + 70 > cardX && fire.x - 28 < cardX && fire.y + 63 > cardY && fire.y - 47 < cardY) {
            fire_eat_card(card_list[i], i);
        }
    }
}

function fire_eat_card(card, i) {
    console.log('fire eat card '+card.num);
    if (card.shadow) {
        card.shadow.kill();
    }
    card.kill();
    card_list.splice(i, 1);
    add_heart_beat(fire_eat_card_add_heartbeat);
    console.log(card_list);

    if (card_list.length === 0) {
        game_win();
    } else if (i == 0 && card_list[0].shadow) {
        card_list[0].shadow.kill();
    }
}
