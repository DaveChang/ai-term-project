
// 心跳閃動sub
function _heartbeat_blink(action) {
    var r = game.rnd.integerInRange(1, 5);
    //bloodyMask = game.add.image(game.camera.view.x, game.camera.view.y, 'bloody'+r); //跟隨鏡頭更新，但速度太慢
    bloodyMask = game.add.image(0, 0, 'bloody'+r);
    bloodyMask.alpha = 0.4;
}
function _heartbeat_destroy() {
    if(bloodyMask){
        bloodyMask.destroy();
    }
}

// 心跳閃動
function heartbeat_blink(action) {
    heartbeatFlag = 1;
    if(1 == action){ //快
        _heartbeat_blink();
        game.time.events.add(300, this._heartbeat_destroy, this);
        game.time.events.add(550, this._heartbeat_blink, this);
        game.time.events.add(850, this._heartbeat_destroy, this);
        game.time.events.add(850, function (){heartbeatFlag = 0;}, this);
    }else if(2 == action){ //超快
        _heartbeat_blink();
        game.time.events.add(50, this._heartbeat_destroy, this);
        game.time.events.add(100, this._heartbeat_blink, this);
        game.time.events.add(150, this._heartbeat_destroy, this);
        game.time.events.add(400, this._heartbeat_blink, this);
        game.time.events.add(550, this._heartbeat_destroy, this);
        game.time.events.add(550, function (){heartbeatFlag = 0;}, this);
    }
}


function heart_beat_sound(type, num) {
    if (0 && type == 1) { //移動速率
        if (num > 1000) {
            s_all.play('heartBeat3');
        } else if (num > 500) {
            s_all.play('heartBeat2');
        } else {
            s_all.play('heartBeat1');
        }
    }else if (type == 2) { //心律
        if (num > 200) {
            s_all.play('heartBeat3');
        } else if (num > 100) {
            s_all.play('heartBeat2');
        } else {
            s_all.play('heartBeat1');
        }
    }
}

function add_heart_beat(num){
    heartBeatAdd2 += parseInt(num);
    heartBeat = heartBeatAdd1 + heartBeatAdd2;

    if (heartBeat >= heartBeatMax) {
        game_over('死於心臟病發作');
    } else if (heartBeat <= heartBeatMin) {
        heartBeat = heartBeatMin;

    }
}

function check_heart_beat() {
    var nowSpeed = get_final_speed();

    var keys = Object.keys(speed2HeartBeat);
    var max = keys.length -1;
    var maxKey = keys[max];

    if(nowSpeed >=  maxKey){
        console.log('in');
        heartBeatAdd1 =  speed2HeartBeat[maxKey].Value;
    }

    if(speed2HeartBeat[nowSpeed] != undefined){
        heartBeatAdd1 =  speed2HeartBeat[nowSpeed].Value;
    }
    heartBeat = heartBeatAdd1 + heartBeatAdd2;

    if(heartBeat >= heartBeatKeepMin && heartBeat <= heartBeatKeepMax){
        add_score(5);
    }

    if (heartBeat >= heartBeatMax) {
        game_over('死於心臟病發作');
    } else if (heartBeat <= heartBeatMin) {
        heartBeat = heartBeatMin;
    }

}
