

function create_gunbox () {
gunbox = game.add.physicsGroup(Phaser.Physics.ARCADE);
    for (var i = 14; i <= 15; i ++) {
        var card_location = cardLocation[i];
        var X = card_location[0];
        var Y = card_location[1];
        
        var k = gunbox.create(
            X,
            Y,
            'cards',
            22
        );
        k.num = i;
        k.width = 35;
        k.height = 47;
        k.body.immovable = true;
    }
}


// 拿到水槍子彈

function getWaterGunHandler (player, gunbox) {
    s_all.play('getItem');
    gunbox.kill();
    Ammos += 15;
}


// 水球碰到火s 滅火

function extinguishingHandler (bullets, fire) {
    s_all.play('hitFire');
    bullets.kill();
    fire.kill();
    get_explosion(fire.x, fire.y);

}


function init_bullets () {

    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

}



function fireBullet () {


    bullet = bullets.getFirstExists(false);
    fire_time = 300; // 多久可以發射一枚飛彈

    if ( Ammos > 0 && bullet && (game.time.now > bulletTime))
    {
        //  And fire it
        Ammos--;
        if(direction === 0){ //往上飛
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -800;
            bulletTime = game.time.now + fire_time;
            bullet.angle = 270;

        }else if (direction === 1){//往下
            bullet.reset(player.x, player.y - 8);
            bullet.body.velocity.y = +800;
            bulletTime = game.time.now + fire_time;
            bullet.angle = 90;
        }else if (direction === 2){//往左
            bullet.reset(player.x + 8 , player.y);
            bullet.body.velocity.x = -800;
            bulletTime = game.time.now + fire_time;
            bullet.angle = 180;

        }else if (direction === 3){//往右
            bullet.reset(player.x - 8 , player.y);
            bullet.body.velocity.x = +800;
            bulletTime = game.time.now + fire_time;
            bullet.angle = 0;
        }
        // 射水槍  減心跳緊張
        add_heart_beat(-5);
    }
}
