var StoryMode = 1;
var obstacleMode = 1;
var Splash = function () {};

Splash.prototype = {

    loadScripts: function () {
        game.load.script('style', 'lib/style.js');
        game.load.script('mixins', 'js/mixins.js');
        //game.load.script('WebFont', 'vendor/webfontloader.js');
        game.load.script('menu', 'js/GameMenu.js');
        game.load.script('common', 'js/common.js');
        game.load.script('water_gun', 'js/water_gun.js');
        game.load.script('fire', 'js/fire.js');
        game.load.script('heartbeat', 'js/heartbeat.js');
        game.load.script('game', 'js/ctrl.js');
        game.load.script('speed2HartBeat', 'js/speed2HartBeat.js');

        game.load.script('card','js/card.js');
        game.load.script('story','js/story.js');
        game.load.script('credits', 'js/credits.js');
        game.load.script('options', 'js/options.js');
    },

    loadBgm: function () {
        // thanks Kevin Macleod at http://incompetech.com/
        //game.load.audio('dangerous', 'assets/bgm/Dangerous.mp3');
        //game.load.audio('exit', 'assets/bgm/Exit the Premises.mp3');
    },

    // varios freebies found from google image search
    loadImages: function () {
        game.load.image('menu-bg', 'assets/images/menu-bg.jpg');
        game.load.image('options-bg', 'assets/images/options-bg.jpg');
        game.load.image('gameover-bg', 'assets/images/gameover-bg.jpg');

        if(obstacleMode){
            preload_obstacle();
        }
    },

    loadFonts: function () {
        WebFontConfig = {
            custom: {
                families: ['TheMinion'],
                urls: ['assets/style/theminion.css']
            }
        }
    },

    init: function () {
        this.loadingBar = game.make.sprite(game.world.centerX-(387/2), 400, "loading");
        this.logo       = game.make.sprite(game.world.centerX, 200, 'brand');
        this.logo.scale.x = 2;
        this.logo.scale.y = 2;
        this.status     = game.make.text(game.world.centerX, 380, 'Loading...', {fill: 'white'});
        utils.centerGameObjects([this.logo, this.status]);
    },

    preload: function () {
        game.add.sprite(0, 0, 'stars');
        game.add.existing(this.logo).scale.setTo(0.5);
        game.add.existing(this.loadingBar);
        game.add.existing(this.status);
        this.load.setPreloadSprite(this.loadingBar);

        this.loadScripts();
        this.loadImages();
        this.loadFonts();
        this.loadBgm();
    },

    addGameStates: function () {
        game.state.add("GameMenu", GameMenu);
        game.state.add("Story", Story);
        game.state.add("GameJam", GameJam);
        //game.state.add("GameOver",GameOver);
        game.state.add("Credits", Credits);
    },

    //addGameMusic: function () {
    //    music = game.add.audio('dangerous');
    //    music.loop = true;
    //    music.play();
    //},

    create: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.refresh();

        this.status.setText('Ready!');
        this.addGameStates();
        //this.addGameMusic();


        if(StoryMode){
        setTimeout(function () {
            game.state.start("GameMenu");
        }, 1000);
        }else{
            game.state.start("GameJam");
        }
    }
};
